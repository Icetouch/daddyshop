<!DOCTYPE html>
<html lang="en">
<head>
    @include('common.header')
</head>
<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch (e){}
    </script>
    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler">
            <span class="sr-only">Toggle sidebar</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        {{--header左侧begin--}}
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    豆爸管理
                </small>
            </a>
        </div>
        {{--header左侧end--}}

        {{--header右侧begin--}}
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="{{asset('avatars/user.jpg')}}" alt="Jason's Photo" />
                            <span class="user-info" style="max-width:150px;">
                                <small>欢迎,</small>
                                {{$adminUser->admin_name}}
                            </span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>
                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="javascript:void(0);" onclick="changeSelfPassword();">
                                <i class="ace-icon fa fa-cog"></i>
                                修改密码
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/logout">
                                <i class="ace-icon fa fa-power-off"></i>
                                注销
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        {{--header右侧end--}}
    </div>
</div>
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>
    <div id="sidebar" class="sidebar responsive">
        <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
        </script>
        <ul class="nav nav-list">
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-users"></i>
                    <span class="menu-text">业主管理</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/owner/index" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            业主信息
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/owner/initownerimport" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            业主信息EXCEL导入
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/owner/initpropertyimport" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            物业数据EXCEL导入
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text">系统管理</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/user" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            管理员
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/role" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            角色
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/company" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            站点
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#" data-target="ajax" data-url="/admin/owner/operate" onclick="loadPage(this)" class="">
                            <i class="menu-icon fa fa-caret-right"></i>
                            最近操作记录
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <!-- /section:basics/sidebar.layout.minimize -->
        <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
        </script>
    </div>
    {{--sidebar end--}}
    <div class="main-content">
        <div class="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
            <ul  id="breadcrumbs" class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                        <span class="blue">
                                主页
                        </span>
                </li>
            </ul><!-- /.breadcrumb -->
        </div>
        <!-- /section:basics/content.breadcrumbs -->
        <div class="page-content">
            <div class="page-content-area">
                <!-- ajax content goes here -->
            </div><!-- /.page-content-area -->
        </div><!-- /.page-content -->
    </div>
</div>
@include('common.middle')
<script type="text/javascript">

    function updateBreadcrumb() {
        var breadcrumb_items = [];
        $(this).parents('.nav-list li').each(function () {
            var link = $(this).find('> a');
            var text = link.text();
            var href = link.attr('href');
            breadcrumb_items.push({'text': text, 'href': href});
        });


        var breadcrumbs = $("#breadcrumbs");
        breadcrumbs.empty();
        var li = $("<li>").appendTo(breadcrumbs);
        $("<i>", {"class": "ace-icon fa fa-home home-icon"}).appendTo(li);
        $("<span>", {html: " 主页"}).appendTo(li);

        if (breadcrumb_items.length > 0) {
            for (var i = breadcrumb_items.length; i--; i > 0) {
                var item = breadcrumb_items[i];
                if (i > 0) {
                    $("<li>").append($("<i>", {"class": "ace-icon fa"})).append($("<span>", {html: item.text})).appendTo(breadcrumbs);
                } else {
                    $("<li>", { html: item.text,class:"blue"}).appendTo(breadcrumbs);
                }
            }
        }
    }
    function loadPage(menu) {
        var li = $(menu).parent();
        $(".nav-list li.active").removeClass('active');
        li.addClass('active').parents('.nav-list li').addClass('active');
        var link = $(menu).attr("data-url");
        var target=$(menu).attr("data-target");
        var area = $(".page-content-area");
        area.empty();
        if(target=="ajax"){
            area.load(link,function(response,status,xhr){
                if(status!="success"){
                    $(".page-content-area").html(response);
                }
            });
            //更新面包屑
            updateBreadcrumb.call(menu);
        }
        else if(target=="iframe"){
            area.append("<iframe style='border:none;' id='iframepage' src='"+link+"' width='100%'></iframe>");
            $("#iframepage").height($("html").height()-170);
        }
    }

    //解决bootstrap modal堆叠问题
    $(document)
            .on('show.bs.modal', '.modal', function(event) {
                $(this).appendTo($('body'));
            })
            .on('shown.bs.modal', '.modal.in', function(event) {
                setModalsAndBackdropsOrder();
            })
            .on('hidden.bs.modal', '.modal', function(event) {
                setModalsAndBackdropsOrder();
                if ($('.modal.in').length == 0) {
                    $('body').removeClass('modal-open');
                }
            });

    function setModalsAndBackdropsOrder() {
        $('body').addClass('modal-open');
        var modalZIndex = $('.modal.in').length + 1050 + 1;
        var backdropZIndex = modalZIndex - 1;
        //$('.modal-backdrop').addClass('hidden');
        $('.modal.in:last').css('z-index', modalZIndex);
        $('.modal-backdrop.in:last').css('z-index', backdropZIndex);//.removeClass('hidden');
    }
    function changeSelfPassword(){
        var arg = {};
        arg.data = {title: "修改密码", url: "/admin/modifyPass"};
        getPageInModel(arg);
    }

    function getPageInModel(event) {
//        $("body").showLoading();
        $.ajax({
            type: 'GET',
            url: event.data.url,
            success: function (data) {
//                $("body").hideLoading();
                bootbox.dialog(
                        {
                            title: event.data.title,
                            className: "pageModel",
                            message: data,
                            animate:false
                        });
                if(event.data.func!=null){
                    event.data.func();
                }
            },
            error:function(xmlHttpRequest,textStatus,errorThrown){
                bootbox.dialog(
                        {
                            title: xmlHttpRequest.status,
                            className: "pageModel",
                            message: xmlHttpRequest.responseText
                        });
//                $("body").hideLoading();
            }
        });
    }

    $.fn.datepicker.dates['en'] = {
        days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
        daysShort: ["日", "一", "二", "三", "四", "五", "六", "七"],
        daysMin: ["日", "一", "二", "三", "四", "五", "六", "七"],
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        today: "今天",
        clear: "清除"
    };
</script>

</body>
</html>
