<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="col-xs-12">
    <form id="changePasswordForm" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="col-sm-3 control-label" for="curPassword">当前密码：</label>
            <div class="col-sm-9">
                <input type="password" id="curPassword" name="curPassword" class="form-control required" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="newPassword">输入新密码：</label>
            <div class="col-sm-9">
                <input type="password" id="newPassword" name="newPassword" class="form-control"  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="newPassword1">确认新密码：</label>
            <div class="col-sm-9">
                <input type="password" id="newPassword1" name="newPassword1" class="form-control"  />
            </div>
        </div>
        <div>
            <div class="col-sm-3"></div>
            <div class="col-sm-9"><a href="#" id="changePasswordBtn" class="btn btn-success btn-lg btn-block">保存</a></div>
        </div>
    </form>
</div>
<div class="clearfix"></div>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function changePassword(){
        if($("#newPassword").val()!=$("#newPassword1").val()){
            alert("两次输入的新密码不一样，请重新输入！");
            return false;
        }
        if($("#curPassword").val()==$("#newPassword1").val()){
            alert("新密码不能与现有密码一样！");
            return false;
        }
        //提交
        $.ajax("/admin/modifyPass", {
            type: "Post",
            headers: {accept: "application/json"},
            data: $("#changePasswordForm").serialize(),
            success: function (data) {
                if (data.code == 200 || data.code == 301) {
                    alert(data.value);
                    $(".bootbox").remove();
                    $(".modal-backdrop").remove();
                    $("body").removeClass("modal-open");
                } else {
                    alert(data.msg);
                }
            }
        });
    }
    $(function(){
        $("#changePasswordBtn").on("click",changePassword);

    });
</script>