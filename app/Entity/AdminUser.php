<?php
namespace App\Entity;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Created by PhpStorm.
 * User: magic
 * Date: 17/4/10
 * Time: 12:36
 */

class AdminUser extends Model implements AuthenticatableContract, CanResetPasswordContract{
    use Authenticatable, CanResetPassword;

    public $timestamps = false;
    protected $table = 'admin';
    protected $primaryKey = 'admin_id';
    protected $fillable = ['admin_id','account','password','admin_name','tel'];
}