<?php
/**
 * Created by PhpStorm.
 * User: magic
 * Date: 17/4/10
 * Time: 12:25
 */

namespace App\Http\Controllers;


use App\Entity\AdminUser;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller{

    use AuthenticatesAndRegistersUsers;

    public function login(){
        $username = Input::get('username');
        $password = Input::get('password');
        if(Auth::attempt(array('account'=>$username,'password'=>$password),false)){
            $admin_user = AdminUser::where('account',$username)->first();
            Session::put('uid',$admin_user->admin_id);
            return view('sysmanage.sysindex')->with("adminUser",$admin_user);
        }else{
            return Redirect::to('/')->with('message','用户名或密码错误')->withInput();
        }
    }

    public function modifyPass(){
        $uid = Session::get('uid');
        $adminUser = AdminUser::find($uid);
        $curPassword = Input::get('curPassword');
        $newPassword = Input::get('newPassword');
        $message = '';
        if(password_verify($curPassword, $adminUser->password)){
            $adminUser->password = Hash::make($newPassword);
            $adminUser->save();
            $message['value'] = '密码更新成功';
            $message['code'] = 200;
            $message['msg'] = '';
        }else{
            $message['value'] = '密码更新失败，原密码错误';
            $message['code'] = 200;
            $message['msg'] = '';
        }
        return response()->json($message);
    }

    public function logout(){
        Auth::logout();
        Session::forget('uid');
        return Redirect::to('/');
    }
}